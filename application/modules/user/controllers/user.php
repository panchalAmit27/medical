<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Ion_auth');
        $this->load->library('session');
        $this->load->library('form_validation');
        
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        
        $this->load->model('user_model');
       
        $this->load->model('sms/sms_model');
        $this->load->library('upload');
        $this->load->model('ion_auth_model');
        $this->db->where('hospital_id', 'superadmin');
        $language = $this->db->get('settings')->row()->language;
        $this->lang->load('system_syntax', $language);       
        $this->load->model('settings/settings_model');
        
        $this->load->model('hospital/hospital_model');
        $this->hospital_id = $this->hospital_model->hospitalId();
        if (!$this->ion_auth->in_group('admin')) {
            redirect('home/permission');
        }

       
    }

    public function index() {        
        $this->db->select('*');
        $user_id = $this->ion_auth->get_user_id();        
        $this->db->where('hospital_ion_id', $user_id);
        $data['users'] = $this->db->get('users')->result();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('user', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addNewView() {
        $names = array('superadmin', 'admin');
        $this->db->select('*');
        $this->db->where_not_in('name', $names);
        $query =  $this->db->get('groups');
        $data['groups'] = $query->result();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_new',$data);
        $this->load->view('home/footer'); // just the header file
    }
    
     public function editNewView() {
        $data = array();
        $id = $this->input->post('id');
        $group_id = $this->db->get_where('users_groups', array('user_id' => $id))->row()->group_id;
        $data['user'] = $this->user_model->getUserById($id);
        $names = array('superadmin', 'admin');
        $this->db->select('*');
        $this->db->where_not_in('name', $names);
        $query =  $this->db->get('groups');
        $data['groups'] = $query->result();
        $data['group_id'] = $group_id;
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');
        $username = $fname.' '.$lname;
        
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // Validating First Name Field
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[5]|max_length[100]|xss_clean');
        // Validating Last Name Field
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|min_length[5]|max_length[100]|xss_clean');        
        
        // Validating Phone Field           
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]|max_length[50]|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('editNew', $data);
            $this->load->view('home/footer'); // just the footer file
        } else {
            $data = array();            
            $hospital_ion_id = $this->ion_auth->get_user_id();
            $user_type = $this->input->post('user_type');
            $data = array(
                'first_name' => $fname,
                'last_name' => $lname,
                'username' => $fname . ' ' . $lname,                
                'phone' => $phone,                
                'hospital_ion_id' => $hospital_ion_id,
                'created_on' => time()
            );


            $this->user_model->updateUser($id, $data);            
            $this->ion_auth_model->remove_from_group($user_type, $id);
            $this->ion_auth_model->add_to_group($user_type, $id);
            $this->session->set_flashdata('feedback', 'User Updated');

            // Loading View
            redirect('user');
        }
    }

    public function addNew() {
        $id = $this->input->post('id');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');
        $username = $fname.' '.$lname;
        
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // Validating First Name Field
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100]|xss_clean');
        // Validating Last Name Field
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|min_length[3]|max_length[100]|xss_clean');
        // Validating Password Field
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_length[100]|xss_clean');
        // Validating Email Field
        $this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[3]|max_length[100]|xss_clean');
        // Validating Phone Field           
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[10]|max_length[50]|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $data = array();
                $id = $this->input->get('id');
                $group_id = $this->db->get_where('users_groups', array('user_id' => $id))->row()->group_id;

                $data['user'] = $this->user_model->getUserById($id);
                $names = array('superadmin', 'admin');
                $this->db->select('*');
                $this->db->where_not_in('name', $names);
                $query =  $this->db->get('groups');
                $data['groups'] = $query->result();
                $data['group_id'] = $group_id;
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('editNew', $data);
                $this->load->view('home/footer'); // just the footer file
            } else {
                
                $names = array('superadmin', 'admin');
                $this->db->select('*');
                $this->db->where_not_in('name', $names);
                $query =  $this->db->get('groups');
                $data['groups'] = $query->result();
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_new',$data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            //$error = array('error' => $this->upload->display_errors());
            $data = array();
            $password = $this->ion_auth_model->hash_password($password);
            $hospital_ion_id = $this->ion_auth->get_user_id();
            $user_type = $this->input->post('user_type');
            $data = array(
                'first_name' => $fname,
                'last_name' => $lname,
                'username' => $fname.' '.$lname,
                'email' => $email,
                'phone' => $phone,
                'password' => $password,
                'hospital_ion_id' => $hospital_ion_id,
                'created_on' => time()
            );
            
            // Adding New Hospital
                if ($this->ion_auth->email_check($email)) {
                    $this->session->set_flashdata('feedback', 'This Email Address Is Already Registered');
                    redirect('user/addNewView');
                } else {     
                    $this->user_model->insertUser($data);
                    $user_id = $this->db->insert_id();
                    $this->ion_auth_model->add_to_group($user_type,$user_id);
                    $this->session->set_flashdata('feedback', 'New User Created');
                }            
            // Loading View
            redirect('user');
        }
    }

    function getUser() {
        $data['users'] = $this->user_model->getUser();
        $this->load->view('user', $data);
    }

    function activate() {
        $user_id = $this->input->get('user_id');
        $data = array('active' => 1);
        $this->user_model->activate($user_id, $data);
        $this->session->set_flashdata('feedback', 'Activated');
        redirect('user');
    }

    function deactivate() {
        $user_id = $this->input->get('user_id');
        $data = array('active' => 0);
        $this->user_model->deactivate($user_id, $data);
        $this->session->set_flashdata('feedback', 'Deactivated');
        redirect('user');
    }

    function editUser() {
        $data = array();
        $id = $this->input->get('id');
        $group_id = $this->db->get_where('users_groups', array('user_id' => $id))->row()->group_id;
        
        $data['user'] = $this->user_model->getUserById($id);
        $names = array('superadmin', 'admin');
        $this->db->select('*');
        $this->db->where_not_in('name', $names);
        $query =  $this->db->get('groups');
        $data['groups'] = $query->result();
        $data['group_id'] = $group_id;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('editNew', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function editUserByJason() {
        $id = $this->input->get('id');
        $data['user'] = $this->user_model->getUserById($id);
        echo json_encode($data);
    }

    function delete() {
        $id = $this->input->get('id');
        $this->db->where('user_id', $id);
        $this->db->delete('users_groups');
        $this->db->where('id', $id);
        $this->db->delete('users');
        redirect('user');
    }

}

/* End of file hospital.php */
/* Location: ./application/modules/hospital/controllers/hospital.php */
