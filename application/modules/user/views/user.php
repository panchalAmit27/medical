<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                <i class="fa fa-hospital-o"></i>  <?php echo lang('all_users'); ?>
            </header>

            <style>
                .editbutton{
                    width: auto !important;
                }

                .delete_button{
                    width: auto !important;
                }

                .status{
                    background: #123412 !important;
                }

            </style>



            <div class="panel-body">
                <div class="adv-table editable-table">
                    <div class="clearfix no-print">
                        <a data-toggle="modal" href="user/addNewView">
                            <div class="btn-group">
                                <button id="" class="btn green">
                                    <i class="fa fa-plus-circle"></i>   <?php echo lang('create_new_user'); ?>
                                </button>
                            </div>
                        </a>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th> <?php echo lang('fname'); ?></th>
                                <th> <?php echo lang('lname'); ?></th>
                                <th> <?php echo lang('user_title'); ?></th>
                                <th> <?php echo lang('email'); ?></th>
                                <th> <?php echo lang('phone'); ?></th>
                                <th> <?php echo lang('status'); ?></th>
                                <th class="no-print"> <?php echo lang('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        <style>

                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }

                        </style>

                        <?php
                        foreach ($users as $user) {
                           
                                ?>
                                <tr class="">
                                    <td> <?php echo $user->first_name; ?></td>
                                    <td> <?php echo $user->last_name; ?></td>
                                    <td> <?php echo $user->username; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php echo $user->phone; ?></td>
                                    
                                    <td>
                                        <?php
                                        $status = $this->db->get_where('users', array('id' => $user->id))->row()->active;
                                        if ($status == '1') {
                                            ?>
                                            <button type="button" class="btn btn-info btn-xs btn_width" data-toggle="modal" data-id="<?php echo $user->id; ?>"><?php echo lang('active'); ?></button> 
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-info btn-xs delete_button" data-toggle="modal" data-id="<?php echo $user->id; ?>"><?php echo lang('disabled'); ?></button> 
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="no-print">
                                        <?php
                                        
                                        if ($user->active == '1') {
                                            ?>
                                            <a href="user/deactivate?user_id=<?php echo $user->id; ?>" type="button" class="btn btn-info btn-xs status" data-toggle="modal" data-id="<?php echo $user->id; ?>"><?php echo lang('disable'); ?></a>  

                                        <?php } else {
                                            ?>

                                            <a href="user/activate?user_id=<?php echo $user->id; ?>" type="button" class="btn btn-info btn-xs status" data-toggle="modal" data-id="<?php echo $user->id; ?>"><?php echo lang('enable'); ?></a>  
                                            <?php
                                        }
                                        ?>
                                        <a type="button" class="btn btn-info btn-xs btn_width" data-toggle="" href="user/editUser?id=<?php echo $user->id; ?>" data-id="<?php echo $user->id; ?>"><i class="fa fa-edit"></i></a>   
                                        <a class="btn btn-info btn-xs btn_width delete_button" href="user/delete?id=<?php echo $user->id; ?>" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $('#editable-sample').DataTable({
            responsive: true,

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [1, 2, 3, 4],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: -1,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_ records per page",
            }


        });
    });
</script>

<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>