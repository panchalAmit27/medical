
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                <?php
                if (!empty($user->id)) {
                    echo '<i class="fa fa-edit"></i> ' . lang('edit_user');
                } else {
                    echo '<i class="fa fa-plus-circle"></i> ' . lang('add_new_user');
                }
                ?>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">

                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="col-lg-12">
                                        <div class="col-lg-3"></div>
                                        <div class="col-lg-6">
                                            <?php echo validation_errors(); ?>
                                            <?php echo $this->session->flashdata('feedback'); ?>
                                        </div>
                                        <div class="col-lg-3"></div>
                                    </div>
                                    <form role="form" action="user/editNewView" method="post" enctype="multipart/form-data">
                                        <div class="form-group">


                                            <label for="exampleInputEmail1"><?php echo 'First Name'; ?></label>
                                            <input type="text" class="form-control" name="fname" id="exampleInputFname" value='<?php
                                            if (!empty($user->first_name)) {
                                                echo $user->first_name;
                                            }
                                            ?>' placeholder="">

                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo 'Last Name'; ?></label>
                                            <input type="text" class="form-control" name="lname" id="exampleInputLname" value='<?php
                                            if (!empty($user->last_name)) {
                                                echo $user->last_name;
                                            }
                                            ?>' placeholder="">
                                        </div>
                                        
                                        <div class="form-group">
                                            
                                                <label for="exampleInputEmail1"> <?php echo lang('group'); ?></label>
                                            
                                             <select class="form-control m-bot15 js-example-basic-single pos_select" id="user_type" name="user_type" value=''> 
                                                    <option value="">Select .....</option>
                                                    <?php foreach ($groups as $group) { ?>
                                                        <option value="<?php echo $group->id;?>" 
                                                        
                                                            <?php if ($group_id == $group->id) {
                                                                echo 'selected';
                                                            }
                                                         ?>> <?php echo $group->name; ?>
                                                        </option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                                            <input type="text" class="form-control" name="email" id="exampleInputEmail1" value='<?php
                                            if (!empty($user->email)) {
                                                echo $user->email;
                                            }
                                            ?>' placeholder="" readonly="">
                                        </div>
                                                                                                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                                            <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='<?php
                                            if (!empty($user->phone)) {
                                                echo $user->phone;
                                            }
                                            ?>' placeholder="">
                                        </div>
                                      
                                        <input type="hidden" name="id" value='<?php
                                        if (!empty($user->id)) {
                                            echo $user->id;
                                        }
                                        ?>'>

                                        <button type="submit" name="submit" class="btn btn-info"><?php echo lang('submit'); ?></button>
                                    </form>

                                </div>
                            </section>
                        </div>

                    </div>

                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->




<script src="common/js/codearistos.min.js"></script>


<script>
    $(document).ready(function () {
<?php
if (!empty($user->id)) {
    if (empty($user->package)) {
        ?>
                $('.pos_client').show();
    <?php } else { ?>
                $('.pos_client').hide();
        <?php
    }
} else {
    ?>
            $('.pos_client').hide();
<?php } ?>
        $(document.body).on('change', '#pos_select', function () {

            var v = $("select.pos_select option:selected").val()
            if (v == '') {
                $('.pos_client').show();
            } else {
                $('.pos_client').hide();
            }
        });

    });
</script>